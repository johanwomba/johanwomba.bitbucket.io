var canvas = document.getElementById('canvas'),
    context = canvas.getContext('2d'),
    radius = 15,
    dragging = false;

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

context.lineWidth = radius*2;

var draw = function() {
  var x, y;
  if (dragging) {
    if (event.type == 'mousedown' || event.type == 'mousemove') {
      x = event.x;
      y = event.y;
    }
    else if (event.type == 'touchstart' || event.type == 'touchmove') {
      var touch = event.touches[0];
      x = touch.pageX;
      y = touch.pageY;
    }
    context.lineTo(x, y);
    context.stroke();
    context.beginPath();
    context.arc(x, y, radius, 0, Math.PI*2);
    context.fill();
    context.beginPath();
    context.moveTo(x, y);
  }
}

var engage = function() {
  dragging = true;
  draw();
}
var disengage = function() {
  dragging = false;
  context.beginPath();
}
document.body.addEventListener('touchmove', function(event) {
  event.preventDefault();
}, false);

canvas.addEventListener('mousedown', engage);
canvas.addEventListener('mousemove', draw);
canvas.addEventListener('mouseup', disengage);
canvas.addEventListener('touchstart', engage);
canvas.addEventListener('touchmove', draw);
canvas.addEventListener('touchend', disengage);

//color selection
var colors = ['#181818', '#AB4642', '#DC9656', '#A1B56C', '#7CAFC2', '#BA8BAF', '#A16946'];

for(var i=0, n=colors.length; i<n; i++) {
  var swatch = document.createElement('div');
  swatch.className = 'swatch';
  swatch.style.backgroundColor = colors[i];
  swatch.addEventListener('click', setSwatch);
  document.getElementById('colors').appendChild(swatch);
}

function setColor(color) {
  context.fillStyle = color;
  context.strokeStyle = color;
  var active = document.getElementsByClassName('active')[0];
  if (active) {
    active.className = 'swatch';
  }
}
function setSwatch(e) {
  var swatch = e.target;
  setColor(swatch.style.backgroundColor);
  swatch.className += ' active';
}
setSwatch({target: document.getElementsByClassName('swatch')[0]}); //set default swatch on page load.
window.addEventListener("resize", function() {
  location.reload(false);
});
window.addEventListener("orientationchange", function() {
  location.reload(false);
});

var Theremin = function() {

  var canvas;
  var audioContext;
  var oscillator;
  var gain;
  var low = 0;
  var high = 1760;

  var Theremin = function() {
    canvas = createHiDPICanvas(window.innerWidth, window.innerHeight);
    window.AudioContext = window.AudioContext || window.webkitAudioContext;
    audioContext = new window.AudioContext();
    Theremin.setupEventListeners();
  };

  Theremin.setupEventListeners = function() {

    document.body.addEventListener('touchmove', function(event) {
      event.preventDefault();
    }, false);

    window.addEventListener("resize", function() {
    	canvas = createHiDPICanvas(window.innerWidth, window.innerHeight);
    });
    window.addEventListener("orientationchange", function() {
    	canvas = createHiDPICanvas(window.innerWidth, window.innerHeight);
    });

    canvas.addEventListener('mousedown', Theremin.play);
    canvas.addEventListener('touchstart', Theremin.play);

    canvas.addEventListener('touchend', Theremin.stopSound);
    canvas.addEventListener('mouseup', Theremin.stopSound);
    document.addEventListener('mouseleave', Theremin.stopSound);
  };

  Theremin.getSoundType = function(){
    var selector = document.getElementById('radioButton');
    var value = selector[selector.selectedIndex].value;
    if (value == 'sine') {
      return 'sine';
    }
    else if (value == 'sawtooth') {
      return 'sawtooth';
    }
    else if (value == 'triangle') {
      return 'triangle';
    }
    else if (value == 'square') {
      return 'square';
    }
  }

  Theremin.play = function(event) {
    oscillator = audioContext.createOscillator();
    gain = audioContext.createGain();


    oscillator.type = Theremin.getSoundType();

    gain.connect(audioContext.destination);
    oscillator.connect(gain);

    Theremin.updateFrequency(event);

    oscillator.start(0);

    canvas.addEventListener('mousemove', Theremin.updateFrequency);
    canvas.addEventListener('touchmove', Theremin.updateFrequency);

    canvas.addEventListener('touchend', Theremin.stopSound);
    canvas.addEventListener('mouseout', Theremin.stopSound);
    canvas.addEventListener('touchcancel', Theremin.stopSound);
  };

  Theremin.stopSound = function(event) {
    oscillator.stop(0);

    canvas.removeEventListener('mousemove', Theremin.updateFrequency);
    canvas.removeEventListener('touchmove', Theremin.updateFrequency);

    canvas.removeEventListener('touchend', Theremin.stopSound);
    canvas.removeEventListener('mouseout', Theremin.stopSound);
    canvas.removeEventListener('touchcancel', Theremin.stopSound);
  };

  Theremin.getTone = function(x) {
    var noteDifference = high - low;
    var noteOffset = (noteDifference / canvas.offsetWidth) * (x - canvas.offsetLeft);
    return low + noteOffset;
  };
  Theremin.getVolume = function(y) {
    var volumeLevel = 1 - (((100 / canvas.offsetHeight) * (y - canvas.offsetTop)) / 100);
    return volumeLevel;
  };

  Theremin.writeData = function(note, volume) {
    var context = canvas.getContext("2d");
    context.fillStyle = '#383838';
    context.fillRect(0, 0, window.innerWidth, window.innerHeight);
    context.fillStyle = "#F8F8F8";
    context.font = "25px Georgia";
    var noteValue = Math.floor(note) + ' Hz';
    var volumeValue = Math.floor(volume * 100) + '%';
    context.fillText("Tone: " + noteValue + ", Volume: " + volumeValue, 0, 200);
  };

  Theremin.calculateFrequency = function(x, y) {
    var tone = Theremin.getTone(x);
    var volume = Theremin.getVolume(y);
    Theremin.writeData(tone, volume);
    oscillator.frequency.value = tone;
    gain.gain.value = volume;
  };

  Theremin.updateFrequency = function(event) {
    if (event.type == 'mousedown' || event.type == 'mousemove') {
      Theremin.calculateFrequency(event.x, event.y);
    } else if (event.type == 'touchstart' || event.type == 'touchmove') {
      var touch = event.touches[0];
      Theremin.calculateFrequency(touch.pageX, touch.pageY);
    }
  };

  return Theremin;
}();

var PIXEL_RATIO = (function () {
    var ctx = document.getElementById('theremin').getContext("2d"),
        dpr = window.devicePixelRatio || 1,
        bsr = ctx.webkitBackingStorePixelRatio ||
              ctx.mozBackingStorePixelRatio ||
              ctx.msBackingStorePixelRatio ||
              ctx.oBackingStorePixelRatio ||
              ctx.backingStorePixelRatio || 1;

    return dpr / bsr;
})();
createHiDPICanvas = function(w, h, ratio) {
    if (!ratio) { ratio = PIXEL_RATIO; }
    var can = document.getElementById('theremin');
    can.width = w * ratio;
    can.height = h * ratio;
    can.style.width = w + "px";
    can.style.height = h + "px";
    can.getContext("2d").setTransform(ratio, 0, 0, ratio, 0, 0);
    return can;
}
window.onload = function() {
  var theremin = new Theremin();
}

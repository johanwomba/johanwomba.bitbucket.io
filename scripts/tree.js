var canvas = document.getElementById('tree'),
    clear = document.getElementById('clear'),
    context = canvas.getContext('2d'),
    colors = ['#181818', '#282828', '#383838', '#585858', '#B8B8B8', '#AB4642',
     '#DC9656', '#F7CA88', '#86C1B9', '#A1B56C', '#7CAFC2', '#BA8BAF', '#A16946'],
    trees = [];

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

function Tree(color, leafColor) {
  this.color = color;
  this.leafColor = leafColor;
  this.xStart = [];
  this.yStart = [];
  this.xEnd = [];
  this.yEnd = [];
  this.thickness = [];
}
Tree.prototype.drawLine = function(xStart, yStart, xEnd, yEnd, color, leafColor, thickness) {
    /*if (thickness < 3) {
      context.strokeStyle = leafColor;
      context.lineWidth = thickness * 10.5;
    }
    else {*/
      context.strokeStyle = color;
      context.lineWidth = thickness * 1.4;
    //}
    context.beginPath();
    context.moveTo(xStart, yStart);
    context.lineTo(xEnd, yEnd);
    context.closePath();
    context.stroke();
}
Tree.prototype.createTree = function (xStart, yStart, angle, thickness) {
  if (thickness === 0) return;
  var branchLength = getRandom(0, 15);
  tree.xStart.push(xStart);
  tree.yStart.push(yStart);
  tree.thickness.push(thickness);
  var xEnd = xStart + (cos(angle) * thickness * branchLength);
  var yEnd = yStart + (sin(angle) * thickness * branchLength);
  tree.xEnd.push(xEnd);
  tree.yEnd.push(yEnd);
  Tree.prototype.createTree(xEnd, yEnd, angle - getRandom(15,20), thickness - 1);
  Tree.prototype.createTree(xEnd, yEnd, angle + getRandom(15,20), thickness - 1);
}
setInterval(function() {
  if (trees.length > 0) {
    drawTree();
  }
}, 5);

function drawTree() {

  trees.forEach(function(obj) {
    if (obj.xStart.length > 0) {
      obj.drawLine(obj.xStart.shift(), obj.yStart.shift(), obj.xEnd.shift(), obj.yEnd.shift(), obj.color, obj.leafColor, obj.thickness.shift());
    }
    else {
      trees.splice(0, 1);
    }
  });
}
function getRandom(min, max){
	return min + Math.floor(Math.random() * (max + 1 - min));
}
function cos (angle) {
	return Math.cos(degreeConvert(angle));
}
function degreeConvert(angle){
	return angle * (Math.PI / 180.0);
}
function sin (angle) {
	return Math.sin(degreeConvert(angle));
}
window.addEventListener('click', function() {

  tree = new Tree(colors[getRandom(0, 12)], colors[getRandom(0, 12)]);
  tree.createTree(event.clientX, event.clientY, -90, 9); //small = 6, medium = 9, large = 11
  trees.push(tree);
  console.log(trees);
});
clear.addEventListener('click', function(){
  trees = [];
  context.clearRect(0, 0, canvas.width, canvas.height);
});
